
# Functions:


```vb
Function By_AddressBar(ClientID As String, Optional Scope As String = Nothing) As String
Function By_VerificationCode(ClientID As String, Optional Scope As String = Nothing) As String
Function By_UsernameAndPassword(ClientID As String, ClientSecret As String, Username As String, Password As String) As Task(Of JSON_TokenfromUsernamePassword)
Function RenewExpiredToken(TheRefreshToken As String, ClientID As String, ClientSecret As String) As Task(Of JSON_ExchangingVerificationCodeForToken)
Function CheckAccessToken() As Task(Of JSON_CheckAccessToken)
Function RemoteUpload(VideoUrl As String, VideoTitle As String, Optional VideoTags As List(Of String) = Nothing, Optional VideoChannel As DaMutilities.ChannelsEnum? = Nothing, Optional Privacy As DaMutilities.PrivacyEnum? = Nothing) As Task(Of JSON_RemoteUpload)
Function UserInfo(UserID As String) As Task(Of JSON_UserInfo)
Function ListFeaturesVideos(UserID As String, Optional OffSet As Integer = 1) As Task(Of JSON_UserInfo)
Function ListVideos(UserID As String, Optional OffSet As Integer = 1) As Task(Of JSON_ListVideos)
Function DeleteVideo(VideoID As String) As Task(Of Boolean)
Function VideoMetadata(VideoID As String) As Task(Of JSON_VideoItem)
Function EditVideo(VideoID As String, Optional VideoTitle As String = Nothing, Optional VideoTags As List(Of String) = Nothing, Optional VideoChannel As DaMutilities.ChannelsEnum? = Nothing, Optional Privacy As DaMutilities.PrivacyEnum? = Nothing) As Task(Of JSON_EditVideo)
Function APIrateLimits() As Task(Of JSON_APIrateLimits)
Function UploadLocalFile(FileToUpload As Object, UploadType As DClient.UploadTypes, Optional VideoTitle As String = Nothing, Optional VideoTags As List(Of String) = Nothing, Optional VideoChannel As DaMutilities.ChannelsEnum? = Nothing, Optional Privacy As DaMutilities.PrivacyEnum? = Nothing, Optional ReportCls As IProgress(Of ReportStatus) = Nothing, Optional _proxi As ProxyConfig = Nothing, Optional token As Threading.CancellationToken = Nothing) As Task(Of JSON_UploadLocalFile)
Function RevokeAccessToken() As Task(Of JSON_CheckAccessToken)
Function GetVideoDirectLink(VideoID As String) As Task(Of JSON_GetVideoDirectLink)
Function EditVideoPrivacy(VideoID As String, Optional Privacy As DaMutilities.PrivacyEnum? = Nothing) As Task(Of JSON_EditVideo)
```

# Example:

```vb
Dim Clnt As DailymotionSDK.IClient = New DailymotionSDK.DClient("accesstoken")
Dim RSLT = Await Clnt.RemoteUpload("http://unlrvista.org/0/1690.MP4", "videoTitle", Nothing, Nothing, DailymotionSDK.DaMutilities.PrivacyEnum.Public)
Dim RSLT = Await Clnt.UserInfo("userID")
Dim RSLT = Await Clnt.ListFeaturesVideos("userID", 1)
Dim RSLT = Await Clnt.ListVideos("x268d4s", 1)
Dim RSLT = Await Clnt.DeleteVideo(T_DeleteVideoID.Text)
Dim RSLT = Await Clnt.VideoMetadata("x74yyud")
Dim RSLT = Await Clnt.EditVideo("x74yydx", Nothing, Nothing, Nothing, DailymotionSDK.DaMutilities.PrivacyEnum.Public)
Dim RSLT = Await Clnt.APIrateLimits()
Dim frm As New DeQma.FileFolderDialogs.VistaOpenFileDialog With {.Multiselect = False}
If frm.ShowDialog = DialogResult.OK AndAlso Not String.IsNullOrEmpty(frm.FileName) Then
   Dim UploadCancellationToken As New Threading.CancellationTokenSource()
   Dim progressIndicator_ReportCls As Progress(Of DailymotionSDK.ReportStatus) = New Progress(Of DailymotionSDK.ReportStatus)(Sub(ReportClass As DailymotionSDK.ReportStatus)
                                                                                                                                           Label1.Text = String.Format("{0}/{1}", ISisFunctions.Bytes_To_KbMbGb.SetBytes(ReportClass.BytesTransferred), ISisFunctions.Bytes_To_KbMbGb.SetBytes(ReportClass.TotalBytes))
                                                                                                                                           ProgressBar1.Value = CInt(ReportClass.ProgressPercentage)
                                                                                                                                           Label2.Text = If(CStr(ReportClass.TextStatus) Is Nothing, "Uploading...", CStr(ReportClass.TextStatus))
                                                                                                                                       End Sub)
   Dim RSLT = Await Clnt.UploadLocalFile(frm.FileName, DailymotionSDK.DClient.UploadTypes.FilePath, "filename_001", Nothing, Nothing, DailymotionSDK.DaMutilities.PrivacyEnum.Public, progressIndicator_ReportCls, Nothing, UploadCancellationToken.Token)
            DataGridView1.Rows.Add(RSLT.name, RSLT.id, RSLT.dimension, ConvDur(RSLT.duration), RSLT.format, RSLT.acodec, RSLT.bitrate, ConvSze(RSLT.size), RSLT.vcodec)
End If
Dim ScopStr As String = String.Join(" ", [Enum].GetValues(GetType(DailymotionSDK.GetToken.ScopesEnum)).Cast(Of DailymotionSDK.GetToken.ScopesEnum)().Select(Function(x) x.ToString()).ToArray())
Dim thUrl = DailymotionSDK.GetToken.By_AddressBar("xxxxxxx65b06", ScopStr)
Return thUrl.ToString
Dim RSLT = Await Clnt.GetVideoDirectLink("xu7heg")
Dim Get_Token = Await DailymotionSDK.GetToken.By_UsernameAndPassword("xxxxf65b06", "xxxxx7b8633f77be87c", RO_Seting.DailymotionUser, RO_Seting.DailymotionPass)
```